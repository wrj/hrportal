package hrportal

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional


@Api(
        value = 'countries',
        description = 'Country API ( ประเทศ ) V1',
        position = 3,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class CountryController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def errorMessageService

    @ApiOperation(value = 'List Countries',response=Country, responseContainer = 'list',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'Text for search', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'name',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
    ])
    def list() {
        def (result,resultArray) = [[:],[]]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"name"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def dataObj = Country.createCriteria()
        def resultsList = dataObj.list (max: max, offset: offset) {
            or{
                ilike("name","%${searchText}%")
            }
            and {
                eq("status",true)
            }
            order(fieldSort,sortValue)
        }
        def dataTotal = Country.createCriteria()
        def totalRecord = dataTotal.list {
            projections {
                count()
            }
            or{
                ilike("name","%${searchText}%")
            }
            and {
                eq("status",true)
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["name"] = it.getName()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }

    @ApiOperation(value = 'Get Country data',response=Country, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Country id สำหรับ filter', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = Country.get(params["id"])
        def (data,result)=[[:],[status: true]]
        if(dataObj)
        {
            data["id"] = dataObj.getId()
            data["name"] = dataObj.getName()
            result["data"] = data
        }else{
            result["status"] = false
            result["message"] = "No data."
        }
        render result as JSON
    }

    @ApiOperation(value = 'Create Country',response=Country, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'name', value = 'ชื่อประเทศ', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        def dataObj = new Country(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Country \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }


    @ApiOperation(value = 'Update Country', response = Country, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Country id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'name', value = 'ชื่อประเทศ', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def update() {
        def (result, dataObj) = [["status": true], null]
        dataObj = Country.get(params["id"])
        dataObj.properties = params
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Update Country \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete Country', response = Country, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Country id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = ["status":true]
        def dataObj = Country.get(params["id"])
        def countProvince = Province.countByCountryAndStatus(dataObj,true)
        if(countProvince == 0)
        {
            dataObj.setStatus(false)
            if (!dataObj.save(flush: true)) {
                result["status"] = false
                result["message"] = "ไม่สามารถลบประเทศ ${dataObj.getName()} ได้ \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
            }
        }else{
            result["status"] = false
            result["message"] = "ไม่สามารถลบประเทศ ${dataObj.getName()} ได้ มีจังหวัดใช้งานอยู่"
        }

        render result as JSON
    }
}