package hrportal

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.sql.Sql

@Api(
        value = 'personnels',
        description = 'personnels API ( พนักงาน ) V1',
        position = 8,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class PersonnelController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET",create: "POST", update:"PUT",upload:"POST",downloadFile:"GET",delete:"DELETE"]
    def imageService
    def errorMessageService
    def domainService
    def dataService
    def personnelService
    def dataSource
    def departmentDependencyService

    @ApiOperation(value = 'Get Personnel data', response = Personnel, responseContainer = 'json', produces = 'application/json', httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'personnel id สำหรับ filter', paramType = 'path', dataType = 'string', required = true),
    ])
    def show() {
        def dataObj = Personnel.get(params["id"])
        def (data,result) = [[:],[status: false]]
        if(dataObj)
        {
            def (historyNames,incomeDeducts,educations,positions) = [[],[],[],[]]
            data["id"] = dataObj.getId()
            data = domainService.convertDomainToMap(data, dataObj.properties)
            data["imageProfile"] = data["imageProfile"]?:""
            if(data["imageProfile"] != "")
            {
                data["imageProfile"] = imageService.readImage(data["imageProfile"],"profile/${dataObj.getId()}")
            }
            data.remove("historyNames")
            data.remove("personnelNameHistory")
            data.remove("positions")
            data.remove("positionHistory")
            data.remove("positionPersonnels")
            data.remove("personnelLeaveDocument")
            data.remove("fundPersonnels")
            data.remove("personnelFundDetail")
            data.remove("personnelFundHistory")
            data.remove("personnelSSIDetail")
            data.remove("personnelSSIHistory")
            data.remove("personnelDisciplineHistory")
            data.remove("personnelInsigniaHistory")
            data.remove("personnelPayrollHistory")
            data.remove("salaryHistory")
            data.remove("incomeDeduct")
            data.remove("personnelIncomeDeducts")
            data.remove("incomeDeductConfig")
            data.remove("educations")
            data.remove("education")
            data.remove("personnelBanks")
            data.remove("bankConfig")
            data.remove("banks")
            data.remove("nameTitle")
            data["nameTitleName"] = ""
            data["nameTitleId"] = ""
            if(dataObj.getNameTitle())
            {
                data.remove("nameTitle")
                data["nameTitleId"] = dataObj.getNameTitle().getId()
                data["nameTitleName"] = dataObj.getNameTitle().getNameTh()
            }
            data["nameRankName"] = ""
            data["nameRankId"] = ""
            if(dataObj.getNameRank())
            {
                data.remove("nameRank")
                data["nameRankId"] = dataObj.getNameRank().getId()
                data["nameRankName"] = dataObj.getNameRank().getNameTh()
            }
            data.remove("marital")
            data["maritalName"] = ""
            data["maritalId"] = ""
            if(dataObj.getMarital()) {
                data.remove("marital")
                data["maritalId"] = dataObj.getMarital().getId()
                data["maritalName"] = dataObj.getMarital().getName()
            }
            data.remove("salaryType")
            data["salaryTypeName"] = ""
            data["salaryTypeId"] = ""
            if(dataObj.getSalaryType()) {
                data.remove("salaryType")
                data["salaryTypeId"] = dataObj.getSalaryType().getId()
                data["salaryTypeName"] = dataObj.getSalaryType().getName()
            }
            data["bloodGroupName"] = ""
            data["bloodGroupId"] = ""
            if(dataObj.getBloodGroup())
            {
                data.remove("bloodGroup")
                data["bloodGroupId"] = dataObj.getBloodGroup().getId()
                data["bloodGroupName"] = dataObj.getBloodGroup().getName()
            }
            data["eyesColorName"] = ""
            if(dataObj.getEyesColor())
            {
                data["eyesColorId"] = dataObj.getEyesColor().getId()
                data["eyesColorName"] = dataObj.getEyesColor().getName()
            }
            data.remove("nationality")
            data["nationalityId"] = ""
            data["nationalityName"] = ""
            if(dataObj.getNationality())
            {
                data["nationalityId"] = dataObj.getNationality().getId()
                data["nationalityName"] = dataObj.getNationality().getName()
            }
            data.remove("citizenship")
            data["citizenshipId"] = ""
            data["citizenshipName"] = ""
            if(dataObj.getCitizenship())
            {
                data["citizenshipId"] = dataObj.getCitizenship().getId()
                data["citizenshipName"] = dataObj.getCitizenship().getName()
            }
            data.remove("religion")
            data["religionId"] = ""
            data["religionName"] = ""
            if(dataObj.getReligion())
            {
                data["religionId"] = dataObj.getReligion().getId()
                data["religionName"] = dataObj.getReligion().getName()
            }
            data["shiftWorkName"] = "ปกติ"
            if(dataObj.getShiftWork() == 1)
            {
                data["shiftWorkName"] = "shift"
            }
            data["bachelorDegreeCertificate"] = data["bachelorDegreeCertificate"]?:""
            data["bachelorDegreeTranscript"] = data["bachelorDegreeTranscript"]?:""
            data["masterDegreeCertificate"] = data["masterDegreeCertificate"]?:""
            data["masterDegreeTranscript"] = data["masterDegreeTranscript"]?:""
            data["doctorDegreeCertificate"] = data["doctorDegreeCertificate"]?:""
            data["doctorDegreeTranscript"] = data["doctorDegreeTranscript"]?:""
            data["identificationCard"] = data["identificationCard"]?:""
            data["homeRegistration"] = data["homeRegistration"]?:""
            data["medicalCertificate"] = data["medicalCertificate"]?:""
            data["militaryServiceRecord"] = data["militaryServiceRecord"]?:""
            data["changingNotification"] = data["changingNotification"]?:""
            data["recommendationLetter1st"] = data["recommendationLetter1st"]?:""
            data["recommendationLetter2nd"] = data["recommendationLetter2nd"]?:""
            data["licenseToPracticeMedicine"] = data["licenseToPracticeMedicine"]?:""
            data["englishTestScore"] = data["englishTestScore"]?:""
            data["diplomaInField1st"] = data["diplomaInField1st"]?:""
            data["diplomaInField2nd"] = data["diplomaInField2nd"]?:""
            data["actingPosition"] = dataObj.getActingPosition()
            data["competency"] = dataObj.getCompetency()
            data["SSI"] = dataObj.checkSSI()
            data["trainingStatus"] = false
            if(dataObj.getTrainingStatus())
            {
                data["trainingStatus"] = dataObj.getTrainingStatus()
            }
            data["humanDevelopment"] = dataObj.getHumanDevelopment()
            data["training"] = dataObj.getTraining()
            data["scholarship"] = dataObj.getScholarship()
            data["breakTaxAddOn"] = dataObj.getBreakTaxAddOn()
            result["status"] = true
            result["data"] = data
        }else{
            result["message"] = "No Data."
        }
        render result as JSON
    }

    @ApiOperation(value = 'Create Personnel', response = Personnel, responseContainer = 'json', produces = 'application/json', httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'code', value = 'รหัสเอกสาร', defaultValue = '', paramType = 'form', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'firstNameTh', value = 'ชื่อภาษาไทย', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'firstNameEng', value = 'ชื่อภาษาอังกฤษ', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'lastNameTh', value = 'นามสกุลภาษาไทย', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'lastNameEng', value = 'นามสกุลภาษาอังกฤษ', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'nickNameTh', value = 'ชื่อเล่นภาษาไทย', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'nickNameEng', value = 'ชื่อเล่นภาษาอังกฤษ', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'gender', value = 'เพศ male,female', defaultValue = 'male', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'startWorkingDate', value = 'วันที่เริ่มงาน dd/mm/yyyy', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'birthDate', value = 'วันที่เกิด dd/mm/yyyy', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'identifyNo', value = 'เลขบัตรประชาชน', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'dateIssue', value = 'วันออกบัตร dd/mm/yyyy', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'dateExpire', value = 'วันบัตรหมดอายุ dd/mm/yyyy', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'age', value = 'อายุ', defaultValue = '', paramType = 'form', dataType = 'integer', required = false),
            @ApiImplicitParam(name = 'weight', value = 'น้ำหนัก', defaultValue = '', paramType = 'form', dataType = 'double', required = false),
            @ApiImplicitParam(name = 'height', value = 'ส่วนสูง', defaultValue = '', paramType = 'form', dataType = 'double', required = false),
            @ApiImplicitParam(name = 'shiftWork', value = 'ประเภทกะงาน 0 = ปกติ,1 = shift', defaultValue = '0', paramType = 'form', dataType = 'integer', required = true),
            @ApiImplicitParam(name = 'eyesColor', value = 'สีตา', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'nationality', value = 'เชื้อชาติ', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'citizenship', value = 'สัญชาติ', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'nameTitle', value = 'คำนำหน้าชื่อ', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'nameRank', value = 'คำนำหน้าชื่อตำแหน่ง', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'bloodGroup', value = 'หมู่เลือด', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
//        Find Position
        def dataObj = new Personnel(convertMapData(params))
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Personnel \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Update Personnel', response = Personnel, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Personnel id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'code', value = 'รหัสเอกสาร', defaultValue = '', paramType = 'form', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'firstNameTh', value = 'ชื่อภาษาไทย', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'firstNameEng', value = 'ชื่อภาษาอังกฤษ', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'lastNameTh', value = 'นามสกุลภาษาไทย', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'lastNameEng', value = 'นามสกุลภาษาอังกฤษ', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'nickNameTh', value = 'ชื่อเล่นภาษาไทย', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'nickNameEng', value = 'ชื่อเล่นภาษาอังกฤษ', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'gender', value = 'เพศ male,female', defaultValue = 'male', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'startWorkingDate', value = 'วันที่เริ่มงาน dd/mm/yyyy', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'birthDate', value = 'วันที่เกิด dd/mm/yyyy', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'identifyNo', value = 'เลขบัตรประชาชน', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'dateIssue', value = 'วันออกบัตร dd/mm/yyyy', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'dateExpire', value = 'วันบัตรหมดอายุ dd/mm/yyyy', defaultValue = '', paramType = 'form', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'age', value = 'อายุ', defaultValue = '', paramType = 'form', dataType = 'integer', required = false),
            @ApiImplicitParam(name = 'weight', value = 'น้ำหนัก', defaultValue = '', paramType = 'form', dataType = 'double', required = false),
            @ApiImplicitParam(name = 'height', value = 'ส่วนสูง', defaultValue = '', paramType = 'form', dataType = 'double', required = false),
            @ApiImplicitParam(name = 'shiftWork', value = 'ประเภทกะงาน 0 = ปกติ,1 = shift', defaultValue = '0', paramType = 'form', dataType = 'integer', required = true),
            @ApiImplicitParam(name = 'eyesColor', value = 'สีตา', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'nationality', value = 'เชื้อชาติ', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'citizenship', value = 'สัญชาติ', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'nameTitle', value = 'คำนำหน้าชื่อ', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'nameRank', value = 'คำนำหน้าชื่อตำแหน่ง', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
            @ApiImplicitParam(name = 'bloodGroup', value = 'หมู่เลือด', defaultValue = '', paramType = 'form', dataType = 'string', required = false),
    ])
    @Transactional
    def update() {
        def dataObj = Personnel.get(params["id"])
        dataObj.properties = convertMapData(params)
        def result = ["status": true]
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Personnel \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }


    def uploadService
    @ApiOperation(value = 'Upload File', response = Personnel, responseContainer = 'json', produces = 'application/json', httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'imageProfile', value = '', paramType = 'form', dataType = 'file', required = false),
            @ApiImplicitParam(name = 'identificationCard', value = 'บัตรประชาชน', paramType = 'form', dataType = 'file', required = false),
            @ApiImplicitParam(name = 'homeRegistration', value = 'ทะเบียนบ้าน', paramType = 'form', dataType = 'file', required = false),
    ])
    @Transactional
    def upload() {
        def (result,dataObj,personnelId,fileName) = [[status: true],Personnel.get(params["id"]),params["id"],""]
        def folderPath = "/profile/${personnelId}"
        String setField = ""
        if(dataService.checkKey(params,"imageProfile"))
        {
            if(params["imageProfile"] == "delete")
            {
                uploadService.deleteFile(dataObj.getImageProfile(),folderPath)
//                dataObj.setImageProfile("")
                setField = "image_profile = ''"
            }else{
                if(params["imageProfile"] != "")
                {
                    if(dataObj.getImageProfile() != "")
                    {
                        uploadService.deleteFile(dataObj.getImageProfile(),folderPath)
                    }
                    fileName = uploadService.uploadFile(request,"imageProfile",folderPath,"profile_${personnelId}")
//                    dataObj.setImageProfile(fileName)
                    setField = "image_profile = '${fileName}'"
                }
            }
        }
        //        identificationCard
        if(dataService.checkKey(params,"identificationCard")) {
            if(params["identificationCard"] == "delete")
            {
                uploadService.deleteFile(dataObj.getIdentificationCard(),folderPath)
//                dataObj.setIdentificationCard("")
                setField = "identification_card = ''"
            }else{
                if(params["identificationCard"] != "")
                {
                    if(dataObj.getIdentificationCard() != "")
                    {
                        uploadService.deleteFile(dataObj.getIdentificationCard(),folderPath)
                    }
                    fileName = uploadService.uploadFile(request, "identificationCard", folderPath, "identificationCard_${personnelId}")
//                    dataObj.setIdentificationCard(fileName)
                    setField = "identification_card = '${fileName}'"
                }
            }
        }
        //        homeRegistration
        if(dataService.checkKey(params,"homeRegistration")) {
            if(params["homeRegistration"] == "delete")
            {
                uploadService.deleteFile(dataObj.getHomeRegistration(),folderPath)
//                dataObj.setHomeRegistration("")
                setField = "home_registration = ''"
            }else{
                if(params["homeRegistration"] != "")
                {
                    if(dataObj.getHomeRegistration() != "")
                    {
                        uploadService.deleteFile(dataObj.getHomeRegistration(),folderPath)
                    }
                    fileName = uploadService.uploadFile(request, "homeRegistration", folderPath, "homeRegistration_${personnelId}")
//                    dataObj.setHomeRegistration(fileName)
                    setField = "home_registration = '${fileName}'"
                }
            }
        }
        String query = "UPDATE personnel SET ${setField} WHERE id = '${params["id"]}'"
        def db = new Sql(dataSource)
        db.execute(query)
        render result as JSON
    }

    @ApiOperation(value = 'Get Personnel File',response=Personnel, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Personnel id สำหรับ filter', paramType = 'path', dataType = 'string',required=true),
            @ApiImplicitParam(name = 'fileName', value = 'file สำหรับ download', paramType = 'query', dataType = 'string'),
    ])
    def downloadFile()
    {
        String filePath = "${uploadService.getPathResource()}/profile/${params["id"]}/${params["fileName"]}"
        render file: new File (filePath), fileName: "${params["fileName"]}"
    }

    @ApiOperation(value = 'Delete Personnel', response = Personnel, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Personnel id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = [status:true]
        def dataObj = Personnel.get(params["id"])
        dataObj.setStatus(false)
        if(!dataObj.save(flush: true))
        {
            result["status"] = false
            result["message"] = "Failed to Delete Personnel \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    def convertMapData(params) {
        if (dataService.checkKey(params,"nameTitle")) {
            params["nameTitle"] = NameTitle.get(params["nameTitle"])
        }
        if (dataService.checkKey(params,"nameRank")) {
            params["nameRank"] = NameTitle.get(params["nameRank"])
        }
        if (dataService.checkKey(params,"nationality")) {
            params["nationality"] = Nationality.get(params["nationality"])
        }
        if (dataService.checkKey(params,"citizenship")) {
            params["citizenship"] = Citizenship.get(params["citizenship"])
        }
        if (dataService.checkKey(params,"bloodGroup")) {
            params["bloodGroup"] = BloodGroup.get(params["bloodGroup"])
        }
        if (dataService.checkKey(params,"eyesColor")) {
            params["eyesColor"] = EyeColor.get(params["eyesColor"])
        }
        if (dataService.checkKey(params,"startWorkingDate")) {
            params = dataService.convertStringToDate(params, "startWorkingDate")
        }
        if (dataService.checkKey(params,"birthDate")) {
            params = dataService.convertStringToDate(params, "birthDate")
        }
        if (dataService.checkKey(params,"dateIssue")) {
            params = dataService.convertStringToDate(params, "dateIssue")
        }
        if (dataService.checkKey(params,"dateExpire")) {
            params = dataService.convertStringToDate(params, "dateExpire")
        }
        return params
    }

    @ApiOperation(value = 'List Personnel', response = Personnel, responseContainer = 'list', produces = 'application/json', httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'Text for search', defaultValue = '', required = false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1', required = false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting personnel_id,first_name_th,last_name_th,personnel_code', defaultValue = 'personnel_id', required = false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc', required = false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20', required = false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'nameTitle', value = 'คำนำหน้า', defaultValue = '', required = false, paramType = 'query', dataType = 'string'),
    ])
    def list() {
        def (result,resultArray) = [[:],[]]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"nameTh"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def dataObj = Personnel.createCriteria()
        def resultsList = dataObj.list (max: max, offset: offset) {
            or{
                ilike("code","%${searchText}%")
                ilike("firstNameTh","%${searchText}%")
                ilike("firstNameEng","%${searchText}%")
                ilike("lastNameTh","%${searchText}%")
                ilike("lastNameEng","%${searchText}%")
            }
            and {
                eq("status",true)
                if(dataService.checkKey(params,"nameTitle"))
                {
                    eq("nameTitle",NameTitle.get(params["nameTitle"]))
                }
            }
            order(fieldSort,sortValue)
        }
        def dataTotal = NameTitle.createCriteria()
        def totalRecord = dataTotal.list {
            projections {
                count()
            }
            or{
                ilike("code","%${searchText}%")
                ilike("firstNameTh","%${searchText}%")
                ilike("firstNameEng","%${searchText}%")
                ilike("lastNameTh","%${searchText}%")
                ilike("lastNameEng","%${searchText}%")
            }
            and {
                eq("status",true)
                if(dataService.checkKey(params,"nameTitle"))
                {
                    eq("nameTitle",NameTitle.get(params["nameTitle"]))
                }
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["code"] = it.getCode()
            item["firstNameTh"] = it.getNameTh()
            item["firstNameEng"] = it.getNameEng()
            item["lastNameTh"] = it.getNameThSh()
            item["lastNameEng"] = it.getNameEngSh()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }
}
