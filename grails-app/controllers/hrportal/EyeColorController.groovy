package hrportal

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'EyeColors',
        description = 'EyeColors API ( สีตา ) V1',
        position = 5,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class EyeColorController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def errorMessageService

    @ApiOperation(value = 'List EyeColors',response=EyeColor, responseContainer = 'list',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'Text for search', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'name',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
    ])
    def list() {
        def (result,resultArray) = [[:],[]]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"name"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def dataObj = EyeColor.createCriteria()
        def resultsList = dataObj.list (max: max, offset: offset) {
            or{
                ilike("name","%${searchText}%")
            }
            and {
                eq("status",true)
            }
            order(fieldSort,sortValue)
        }
        def dataTotal = EyeColor.createCriteria()
        def totalRecord = dataTotal.list {
            projections {
                count()
            }
            or{
                ilike("name","%${searchText}%")
            }
            and {
                eq("status",true)
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["name"] = it.getName()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }

    @ApiOperation(value = 'Get EyeColor data',response=EyeColor, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'eyeColor id สำหรับ filter', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = EyeColor.get(params["id"])
        def (data,result) = [[:],[status: false]]
        if(dataObj)
        {
            data["id"] = dataObj.getId()
            data["name"] = dataObj.getName()
            result["status"] = true
            result["data"] = data
        }else{
            result["message"] = "No Data."
        }

        render result as JSON
    }

    @ApiOperation(value = 'Create EyeColor',response=EyeColor, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'name', value = 'สีตา', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        def dataObj = new EyeColor(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save EyeColor \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }


    @ApiOperation(value = 'Update EyeColor', response = EyeColor, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'EyeColor id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'name', value = 'สีตา', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def update() {
        def (result, dataObj) = [["status": true], null]
        dataObj = EyeColor.get(params["id"])
        dataObj.properties = params
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Update EyeColor \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete EyeColor', response = EyeColor, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'EyeColor id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = ["status":true]
        def dataObj = EyeColor.get(params["id"])
        dataObj.setStatus(false)
        if (!dataObj.save(flush: true)) {
            result["status"] = false
            result["message"] = "Failed to Delete EyeColor \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }
}