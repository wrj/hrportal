package hrportal

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')

        group "/bloodGroups", {
            "/list"(version:'1.0', controller: "bloodGroup", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "bloodGroup" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "bloodGroup" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "bloodGroup" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "bloodGroup" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/citizenships", {
            "/list"(version:'1.0', controller: "citizenship", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "citizenship" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "citizenship" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "citizenship" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "citizenship" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/countries", {
            "/list"(version: '1.0', controller: "country", action: "list", method: "GET", namespace: 'v1')
            "/"(version: '1.0', controller: "country", action: "create", method: "POST", namespace: 'v1')
            "/$id"(version: '1.0', controller: "country", action: "show", method: "GET", namespace: 'v1')
            "/$id"(version: '1.0', controller: "country", action: "update", method: "PUT", namespace: 'v1')
            "/$id"(version: '1.0', controller: "country", action: "delete", method: "DELETE", namespace: 'v1')
        }

        group "/districts", {
            "/list"(version:'1.0', controller: "district", action: "list", method:"GET", namespace:'v1')
            "/"(version: '1.0', controller: "district", action: "create", method: "POST", namespace: 'v1')
            "/$id"(version: '1.0', controller: "district", action: "show", method: "GET", namespace: 'v1')
            "/$id"(version: '1.0', controller: "district", action: "update", method: "PUT", namespace: 'v1')
            "/$id"(version: '1.0', controller: "district", action: "delete", method: "DELETE", namespace: 'v1')
        }

        group "/eyeColors", {
            "/list"(version:'1.0', controller: "eyeColor", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "eyeColor" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "eyeColor" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "eyeColor" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "eyeColor" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/nameTitles", {
            "/list"(version:'1.0', controller: "nameTitle", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "nameTitle" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "nameTitle" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "nameTitle" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "nameTitle" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/nationalities", {
            "/list"(version:'1.0', controller: "nationality", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "nationality" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "nationality" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "nationality" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "nationality" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/personnels", {
            "/list"(version:'1.0', controller: "personnel", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "personnel" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "personnel" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "personnel" , action: "update", method:"PUT", namespace:'v1')
            "/$id/upload/"(version:'1.0', controller: "personnel" , action: "upload", method:"POST", namespace:'v1')
            "/downloadFile/$id"(version:'1.0', controller: "personnel" , action: "downloadFile", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "personnel" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/provinces", {
            "/list"(version:'1.0', controller: "province", action: "list", method:"GET", namespace:'v1')
            "/"(version: '1.0', controller: "province", action: "create", method: "POST", namespace: 'v1')
            "/$id"(version: '1.0', controller: "province", action: "show", method: "GET", namespace: 'v1')
            "/$id"(version: '1.0', controller: "province", action: "update", method: "PUT", namespace: 'v1')
            "/$id"(version: '1.0', controller: "province", action: "delete", method: "DELETE", namespace: 'v1')
        }

        group "/subDistricts", {
            "/list"(version:'1.0', controller: "subDistrict", action: "list", method:"GET", namespace:'v1')
            "/"(version: '1.0', controller: "subDistrict", action: "create", method: "POST", namespace: 'v1')
            "/$id"(version: '1.0', controller: "subDistrict", action: "show", method: "GET", namespace: 'v1')
            "/$id"(version: '1.0', controller: "subDistrict", action: "update", method: "PUT", namespace: 'v1')
            "/$id"(version: '1.0', controller: "subDistrict", action: "delete", method: "DELETE", namespace: 'v1')
        }

        group "/users", {
            "/"(version:'1.0', controller: "login" , action: "doLogin", method:"POST", namespace:'v1')
            "/logout"(version:'1.0', controller: "login" , action: "logout", method:"POST", namespace:'v1')
            "/checkLogin"(version:'1.0', controller: "login" , action: "checkLogin", method:"GET", namespace:'v1')
        }
    }
}
