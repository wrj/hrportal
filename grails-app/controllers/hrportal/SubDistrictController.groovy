package hrportal


import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'subDistricts',
        description = 'Sub District API ( แขวง ) V1',
        position = 10,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class SubDistrictController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def errorMessageService

    @ApiOperation(value = 'List Sub District',response=SubDistrict, responseContainer = 'list',produces='application/json',httpMethod='GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'subDistrictName',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'districtId', value = 'District Id for filter by district', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
    ])
    @Transactional
    def list() {
        def result = [:]
        def resultArray = []
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"subDistrictName"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def districtId = params["districtId"]?params["districtId"]:""
        def subDistrictObj = SubDistrict.createCriteria()
        def resultsList = subDistrictObj.list (max: max, offset: offset) {
            or {
                ilike("subDistrictName","%${searchText}%")
            }
            and {
                if(districtId != "")
                {
                    eq("district",District.get(districtId))
                }
                eq("status",true)
            }
            order(fieldSort,sortValue)
        }
        def subDistrictTotal = SubDistrict.createCriteria()
        def totalRecord = subDistrictTotal.list {
            projections {
                count()
            }
            or {
                ilike("subDistrictName","%${searchText}%")
            }
            and {
                if(districtId != "")
                {
                    eq("district",District.get(districtId))
                }
                eq("status",true)
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["subDistrictName"] = it.getSubDistrictName()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }

    @ApiOperation(value = 'Get SubDistrict data',response=SubDistrict, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'SubDistrict id สำหรับ filter', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = SubDistrict.get(params["id"])
        def (data,result)=[[:],[status: true]]
        if(dataObj)
        {
            data["id"] = dataObj.getId()
            data["subDistrictName"] = dataObj.getSubDistrictName()
            data["districtId"] = dataObj.getDistrict().getId()
            data["district"] = dataObj.getDistrict().getDistrictName()
            data["province"] = dataObj.getDistrict().getProvince().getProvinceName()
            data["country"] = dataObj.getDistrict().getProvince().getCountry().getName()
            result["data"] = data
        }else{
            result["status"] = false
            result["message"] = "No data."
        }
        render result as JSON
    }

    @ApiOperation(value = 'Create SubDistrict',response=SubDistrict, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'subDistrictName', value = 'ชื่อเขต', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'district', value = 'id เขต', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        params["district"] = District.get(params["district"])
        def dataObj = new SubDistrict(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save SubDistrict \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }


    @ApiOperation(value = 'Update SubDistrict', response = SubDistrict, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'SubDistrict id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'subDistrictName', value = 'ชื่อเขต', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'district', value = 'id เขต', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def update() {
        def (result, dataObj) = [["status": true], null]
        params["district"] = District.get(params["district"])
        dataObj = SubDistrict.get(params["id"])
        dataObj.properties = params
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Update SubDistrict \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete SubDistrict', response = SubDistrict, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'SubDistrict id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = ["status":true]
        def dataObj = SubDistrict.get(params["id"])
        dataObj.setStatus(false)
        if (!dataObj.save(flush: true)) {
            result["status"] = false
            result["message"] = "ไม่สามารถลบแขวง ${dataObj.getSubDistrictName()} ได้ \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }
}