package hrportal

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonSlurper

@Api(
        value = 'districts',
        description = 'District API ( เขต ) V1',
        position = 4,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class DistrictController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def errorMessageService

    @ApiOperation(value = 'List District',response=District, responseContainer = 'list',produces='application/json',httpMethod='GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'districtName',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'provinceId', value = 'Province Id for filter by province', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
    ])
    @Transactional
    def list() {
        def result = [:]
        def resultArray = []
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"districtName"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def provinceId = params["provinceId"]?params["provinceId"]:""
        def districtObj = District.createCriteria()
        def resultsList = districtObj.list (max: max, offset: offset) {
            or {
                ilike("districtName","%${searchText}%")
            }
            and {
                if(provinceId != "")
                {
                    eq("province",Province.get(provinceId))
                }
                eq("status",true)
            }
            order(fieldSort,sortValue)
        }
        def districtTotal = District.createCriteria()
        def totalRecord = districtTotal.list {
            projections {
                count()
            }
            or {
                ilike("districtName","%${searchText}%")
            }
            and {
                if(provinceId != "")
                {
                    eq("province",Province.get(provinceId))
                }
                eq("status",true)
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["districtName"] = it.getDistrictName()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }

    @ApiOperation(value = 'Get District data',response=District, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'District id สำหรับ filter', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = District.get(params["id"])
        def (data,result)=[[:],[status: true]]
        if(dataObj)
        {
            data["id"] = dataObj.getId()
            data["districtName"] = dataObj.getDistrictName()
            data["province"] = dataObj.getProvince().getProvinceName()
            data["provinceId"] = dataObj.getProvince().getId()
            data["country"] = dataObj.getProvince().getCountry().getName()
            result["data"] = data
        }else{
            result["status"] = false
            result["message"] = "No data."
        }
        render result as JSON
    }

    @ApiOperation(value = 'Create District',response=District, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'districtName', value = 'ชื่อเขต', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'province', value = 'id จังหวัด', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        params["province"] = Province.get(params["province"])
        def dataObj = new District(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save District \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }


    @ApiOperation(value = 'Update District', response = District, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'District id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'districtName', value = 'ชื่อเขต', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'province', value = 'id จังหวัด', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def update() {
        def (result, dataObj) = [["status": true], null]
        params["province"] = Province.get(params["province"])
        dataObj = District.get(params["id"])
        dataObj.properties = params
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Update District \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete District', response = District, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'District id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = ["status":true]
        def dataObj = District.get(params["id"])
        def countDis = SubDistrict.countByDistrictAndStatus(dataObj,true)
        if(countDis == 0)
        {
            dataObj.setStatus(false)
            if (!dataObj.save(flush: true)) {
                result["status"] = false
                result["message"] = "ไม่สามารถลบเขต ${dataObj.getDistrictName()} ได้ \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
            }
        }else{
            result["status"] = false
            result["message"] = "ไม่สามารถลบเขต ${dataObj.getDistrictName()} ได้ มีแขวงใช้งานอยู่"
        }

        render result as JSON
    }
}