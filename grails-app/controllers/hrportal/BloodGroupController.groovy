package hrportal

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'BloodGroups',
        description = 'BloodGroups API ( หมู่เลือด ) V1',
        position = 1,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class BloodGroupController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def errorMessageService

    @ApiOperation(value = 'List BloodGroups',response=BloodGroup, responseContainer = 'list',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'Text for search', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'name',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
    ])
    def list() {
        def (result,resultArray) = [[:],[]]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"name"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def dataObj = BloodGroup.createCriteria()
        def resultsList = dataObj.list (max: max, offset: offset) {
            or{
                ilike("code","%${searchText}%")
                ilike("name","%${searchText}%")
            }
            and {
                eq("status",true)
            }
            order(fieldSort,sortValue)
        }
        def dataTotal = BloodGroup.createCriteria()
        def totalRecord = dataTotal.list {
            projections {
                count()
            }
            or{
                ilike("code","%${searchText}%")
                ilike("name","%${searchText}%")
            }
            and {
                eq("status",true)
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["code"] = it.getCode()
            item["name"] = it.getName()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }

    @ApiOperation(value = 'Get BloodGroup data',response=BloodGroup, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'bloodGroup id สำหรับ filter', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = BloodGroup.get(params["id"])
        def (data,result) = [[:],[status: false]]
        if(dataObj)
        {
            data["id"] = dataObj.getId()
            data["code"] = dataObj.getCode()
            data["name"] = dataObj.getName()
            result["status"] = true
            result["data"] = data
        }else{
            result["message"] = "No Data."
        }

        render result as JSON
    }

    @ApiOperation(value = 'Create BloodGroup',response=BloodGroup, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'code', value = 'code หมู่เลือด', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'name', value = 'หมู่เลือด', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        def dataObj = new BloodGroup(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save BloodGroup \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }


    @ApiOperation(value = 'Update BloodGroup', response = BloodGroup, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'BloodGroup id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'code', value = 'code หมู่เลือด', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'name', value = 'หมู่เลือด', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def update() {
        def (result, dataObj) = [["status": true], null]
        def dataJson = request.JSON
        dataObj = BloodGroup.get(params["id"])
        dataObj.properties = params
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Update BloodGroup \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete BloodGroup', response = BloodGroup, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'BloodGroup id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = ["status":true]
        def dataObj = BloodGroup.get(params["id"])
        dataObj.setStatus(false)
        if (!dataObj.save(flush: true)) {
            result["status"] = false
            result["message"] = "Failed to Delete BloodGroup \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }
}
