package hrportal


import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonSlurper

@Api(
        value = 'provinces',
        description = 'Provinces API ( จังหวัด ) V1',
        position = 9,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class ProvinceController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]

    def dataService
    def errorMessageService

    @ApiOperation(value = 'List Provinces',response=Province, responseContainer = 'list',produces='application/json',httpMethod='GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'searchText', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'country', value = 'country id', defaultValue = '',required=true, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'provinceName',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
    ])
    @Transactional
    def list() {
        def result = [:]
        def resultArray = []
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"provinceName"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def provinceObj = Province.createCriteria()
        def resultsList = provinceObj.list (max: max, offset: offset) {
            or {
                ilike("provinceName","%${searchText}%")
            }
            and{
                eq("status",true)
                if(dataService.checkKey(params,"country"))
                {
                    eq("country",Country.get(params["country"]))
                }
            }

            order(fieldSort,sortValue)
        }
        def provinceTotal = Province.createCriteria()
        def totalRecord = provinceTotal.list {
            projections {
                count()
            }
            or {
                ilike("provinceName","%${searchText}%")
            }
            and {
                eq("status",true)
                if(dataService.checkKey(params,"country"))
                {
                    eq("country",Country.get(params["country"]))
                }
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["provinceName"] = it.getProvinceName()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }

    @ApiOperation(value = 'Get Province data',response=Province, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Province id สำหรับ filter', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = Province.get(params["id"])
        def (data,result)=[[:],[status: true]]
        if(dataObj)
        {
            data["id"] = dataObj.getId()
            data["provinceName"] = dataObj.getProvinceName()
            data["countryId"] = dataObj.getCountry()?dataObj.getCountry().getId():""
            data["country"] = dataObj.getCountry()?dataObj.getCountry().getName():""
            result["data"] = data
        }else{
            result["status"] = false
            result["message"] = "No data."
        }
        render result as JSON
    }

    @ApiOperation(value = 'Create Province',response=Province, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'provinceName', value = 'ชื่อจังหวัด', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'country', value = 'id ประเทศ', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        params["country"] = Country.get(params["country"])
        def dataObj = new Province(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Province \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }


    @ApiOperation(value = 'Update Province', response = Province, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Province id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'provinceName', value = 'ชื่อจังหวัด', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'country', value = 'id ประเทศ', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def update() {
        def (result, dataObj) = [["status": true], null]
        params["country"] = Country.get(params["country"])
        dataObj = Province.get(params["id"])
        dataObj.properties = params
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Update Province \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete Province', response = Province, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Province id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = ["status":true]
        def dataObj = Province.get(params["id"])
        def countProvince = District.countByProvinceAndStatus(dataObj,true)
        if(countProvince == 0)
        {
            dataObj.setStatus(false)
            if (!dataObj.save(flush: true)) {
                result["status"] = false
                result["message"] = "ไม่สามารถลบจังหวัด ${dataObj.getProvinceName()} ได้ \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
            }
        }else{
            result["status"] = false
            result["message"] = "ไม่สามารถลบจังหวัด ${dataObj.getProvinceName()} ได้ มีเขตใช้งานอยู่"
        }

        render result as JSON
    }
}
