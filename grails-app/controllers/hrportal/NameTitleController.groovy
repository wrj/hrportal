package hrportal


import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'NameTitles',
        description = 'NameTitles API ( คำนำหน้าชื่อ ) V1',
        position = 6,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class NameTitleController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def errorMessageService
    def dataService
    def domainService

    @ApiOperation(value = 'List NameTitles',response=NameTitle, responseContainer = 'list',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'Text for search', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'nameTh',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'nameType', value = '0 = คำนำหน้าชื่อ,1 = คำนำหน้าชื่อตำแหน่ง', defaultValue = '',required=false, paramType = 'query', dataType = 'integer'),
    ])
    def list() {
        def (result,resultArray) = [[:],[]]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"nameTh"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def dataObj = NameTitle.createCriteria()
        def resultsList = dataObj.list (max: max, offset: offset) {
            or{
                ilike("code","%${searchText}%")
                ilike("nameTh","%${searchText}%")
                ilike("nameEng","%${searchText}%")
                ilike("nameThSh","%${searchText}%")
                ilike("nameEngSh","%${searchText}%")
            }
            and {
                eq("status",true)
                if(dataService.checkKey(params,"nameType"))
                {
                    eq("nameType",params["nameType"].toInteger())
                }
            }
            order(fieldSort,sortValue)
        }
        def dataTotal = NameTitle.createCriteria()
        def totalRecord = dataTotal.list {
            projections {
                count()
            }
            or{
                ilike("code","%${searchText}%")
                ilike("nameTh","%${searchText}%")
                ilike("nameEng","%${searchText}%")
                ilike("nameThSh","%${searchText}%")
                ilike("nameEngSh","%${searchText}%")
            }
            and {
                eq("status",true)
                if(dataService.checkKey(params,"nameType"))
                {
                    eq("nameType",params["nameType"].toInteger())
                }
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["code"] = it.getCode()
            item["nameTh"] = it.getNameTh()
            item["nameEng"] = it.getNameEng()
            item["nameThSh"] = it.getNameThSh()
            item["nameEngSh"] = it.getNameEngSh()
            item["nameType"] = it.getNameType()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }

    @ApiOperation(value = 'Get NameTitle data',response=NameTitle, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'nameTitle id สำหรับ filter', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = NameTitle.get(params["id"])
        def (data,result) = [[:],[status: true]]
        if(dataObj)
        {
            data["id"] = dataObj.getId()
            data = domainService.convertDomainToMap(data,dataObj.properties)
            result["data"] = data
        }else{
            result["message"] = "No Data."
        }

        render result as JSON
    }

    @ApiOperation(value = 'Create NameTitle',response=NameTitle, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'code', value = 'code คำนำหน้าชื่อ', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'nameTh', value = 'ชื่อภาษาไทย', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'nameEng', value = 'ชื่อภาษาอังกฤษ', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'nameThSh', value = 'ชื่อภาษาไทยแบบย่อ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'nameEngSh', value = 'ชื่อภาษาอังกฤษแบบย่อ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'nameType', value = '0 = คำนำหน้าชื่อ,1 = คำนำหน้าชื่อตำแหน่ง', defaultValue = '0', paramType = 'query', dataType = 'integer', required=true),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        def dataObj = new NameTitle(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save NameTitle \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }


    @ApiOperation(value = 'Update NameTitle', response = NameTitle, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'NameTitle id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'code', value = 'code คำนำหน้าชื่อ', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'nameTh', value = 'ชื่อภาษาไทย', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'nameEng', value = 'ชื่อภาษาอังกฤษ', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'nameThSh', value = 'ชื่อภาษาไทยแบบย่อ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'nameEngSh', value = 'ชื่อภาษาอังกฤษแบบย่อ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'nameType', value = '0 = คำนำหน้าชื่อ,1 = คำนำหน้าชื่อตำแหน่ง', defaultValue = '0', paramType = 'query', dataType = 'integer', required=true),
    ])
    @Transactional
    def update() {
        def (result, dataObj) = [["status": true], null]
        dataObj = NameTitle.get(params["id"])
        dataObj.properties = params
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Update NameTitle \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete NameTitle', response = NameTitle, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'NameTitle id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = ["status":true]
        def dataObj = NameTitle.get(params["id"])
        dataObj.setStatus(false)
        if (!dataObj.save(flush: true)) {
            result["status"] = false
            result["message"] = "Failed to Delete NameTitle \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }
}