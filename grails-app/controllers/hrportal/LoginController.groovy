package hrportal

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'login',
        description = 'login API ( เข้าสู่ระบบ ) V1',
        position = 9,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)

class LoginController {

    static namespace = "v1"
    static allowedMethods = [doLogin: "POST",logout:"POST",checkLogin:"GET"]
    def sessionLists = [:]

    @ApiOperation(value = 'Login',response=Personnel, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'code', value = 'รหัสพนักงาน', defaultValue = '', paramType = 'form', dataType = 'string', required=true),
            @ApiImplicitParam(name = 'password', value = 'รหัสผ่าน', defaultValue = '', paramType = 'form', dataType = 'string', required=true),
    ])
    @Transactional
    def doLogin() {
        def result = [status:false]
        def perObj = Personnel.findByCode(params["code"])
        if(perObj)
        {
            result["status"] = true
            result["data"] = perObj
            sessionLists.put(perObj.getCode(),perObj)
        }else{
            result["message"] = "Username Or Password Wrong"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Logout',response=Personnel, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'code', value = 'รหัสพนักงาน', defaultValue = '', paramType = 'form', dataType = 'string', required=true),
    ])
    @Transactional
    def logout() {
        def result = [status: true]
        sessionLists.remove(params["code"])
        render result as JSON
    }

    @ApiOperation(value = 'Check Login',response=Personnel, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'code', value = 'รหัสพนักงาน', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def checkLogin()
    {
        def result = [status:false]
        if(sessionLists.containsKey(params["code"]))
        {
            result["status"] = true
            result["data"] = sessionLists[params["code"]]
        }
        render result as JSON
    }
}
