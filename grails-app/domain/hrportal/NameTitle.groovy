package hrportal

class NameTitle {

    static auditable = true
    String id
    String code
    String nameTh
    String nameEng
    String nameThSh
    String nameEngSh
    Integer nameType
    Boolean status
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code blank: true,nullable: true
        nameTh blank: false,nullable: false
        nameEng blank: false,nullable: false
        nameThSh blank: true,nullable: true
        nameEngSh blank: true,nullable: true
        nameType nullable: true
        status nullable: true
    }

    static mapping = {
        comment "คำนำหน้าชื่อ"
        id generator: "assigned"
        code comment:"รหัส"
        nameTh comment:"ชื่อภาษาไทย"
        nameEng comment:"ชื่อภาษาอังกฤษ"
        nameThSh comment:"ชื่อภาษาไทยแบบย่อ"
        nameEngSh comment:"ชื่อภาษาอังกฤษแบบย่อ"
        nameType comment: "ประเภทคำนำหน้า",defaultValue:"0"
        status comment: "สถานะ Record",defaultValue:"true"
    }

    def generateIDService

    def beforeInsert(){
        status = true
    }

    def beforeValidate() {
        if(!this.id)
        {
            def cObj = NameTitle.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}NAMET_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}NAMET_${sprintf('%05d', idCount+1)}"
        }
    }
}
