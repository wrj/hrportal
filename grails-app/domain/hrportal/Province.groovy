package hrportal

class Province {

    static auditable = true
    String id
    String provinceName
    Boolean status
    Date dateCreated
    Date lastUpdated

    static belongsTo = [country:Country]

    static constraints = {
        provinceName blank: false,nullable: false
        country nullable: true
        status nullable: true,blank: true
    }

    static mapping = {
        comment "จังหวัด"
        id generator: "assigned"
        provinceName comment:"ชื่อจังหวัด"
        status defaultValue:"true",comment: "สถานะ Record"
    }

    def generateIDService

    def beforeInsert(){
        status = true
    }

    def beforeUpdate(){

    }

    def beforeValidate() {
        if(!this.id) {
            def cObj = Province.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}PRO_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}PRO_${sprintf('%05d', idCount+1)}"
        }
    }
}
