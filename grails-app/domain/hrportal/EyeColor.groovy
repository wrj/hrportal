package hrportal

class EyeColor {

    static auditable = true
    String id
    String name
    Boolean status
    Date dateCreated
    Date lastUpdated

    static constraints = {
        name blank: false,nullable: false
        status nullable: true
    }

    static mapping = {
        comment "สีตา"
        id generator: "assigned"
        name comment:"สี"
        status defaultValue:"true",comment: "สถานะ Record"
    }

    def generateIDService

    def beforeInsert(){
        status = true
    }

    def beforeValidate() {
        if(!this.id)
        {
            def cObj = EyeColor.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}EYEC_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}EYEC_${sprintf('%05d', idCount+1)}"
        }
    }
}
