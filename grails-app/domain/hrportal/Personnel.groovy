package hrportal

import grails.converters.JSON
import groovy.sql.Sql

class Personnel {

    String id
    String code
    String firstNameTh
    String firstNameEng
    String lastNameTh
    String lastNameEng
    String nickNameTh
    String nickNameEng
    String imageProfile
    String gender //male,female
    Date startWorkingDate //วันที่เริ่มงาน
    Date birthDate
    String identifyNo //รหัสบัตรประชาชน
    Date dateIssue //วันที่ออกบัตร
    Date dateExpire //วันบัตรหมดอายุ
    Integer age
    Double weight
    Double height
    String identificationCard
    String homeRegistration
    Integer shiftWork
    Boolean status
    Date dateCreated
    Date lastUpdated

    static belongsTo = [eyesColor:EyeColor,nationality:Nationality,citizenship:Citizenship,nameTitle:NameTitle,nameRank:NameTitle,bloodGroup:BloodGroup]

    static constraints = {
        code blank: true,nullable: true
        firstNameTh blank: false,nullable: false
        firstNameEng blank: false,nullable: false
        lastNameTh blank: false,nullable: false
        lastNameEng blank: false,nullable: false
        nickNameTh blank: true,nullable: true
        nickNameEng blank: true,nullable: true
        imageProfile blank: true,nullable: true
        gender blank: false,nullable: false
        startWorkingDate nullable: false
        birthDate nullable: true
        identifyNo blank: false,nullable: false
        dateIssue nullable: false
        dateExpire nullable: false
        age nullable: true
        weight nullable: true
        height nullable: true
        eyesColor nullable: true
        nationality blank: true,nullable: true
        citizenship blank: true,nullable: true
        bloodGroup blank: true,nullable: true
        identificationCard blank: true,nullable: true
        homeRegistration blank: true,nullable: true
        shiftWork nullable: true
        nameTitle nullable: true
        nameRank nullable: true
        status nullable: true
    }

    static mapping = {
        comment "พนักงาน"
        id generator: "assigned"
        code comment: "รหัสเอกสาร"
        firstNameTh comment:"ชื่อภาษาไทย"
        firstNameEng comment:"ชื่อภาษาไทย"
        lastNameTh comment:"นามสกุลภาษาไทย"
        lastNameEng comment:""
        nickNameTh comment: "ชื่อเล่นภาษาไทย"
        nickNameEng comment: ""
        imageProfile comment: "รูป Profile"
        gender comment: "เพศ male,female"
        startWorkingDate comment: "วันที่เริ่มงาน"
        resignateReason comment: "เหตุผลที่ออก"
        birthDate comment: "วันเกิด"
        identifyNo comment: "รหัสบัตรประชาชน"
        dateIssue comment: "วันที่ออกบัตร"
        dateExpire comment: "วันที่บัตรหมดอายุ"
        age comment: "อายุ"
        weight comment: "น้ำหนัก"
        height comment: "ส่วนสูง"
        eyesColor comment: "สีตา"
        nationality comment: "เชื้อชาติ"
        citizenship comment: "สัญชาติ"
        religion comment: "ศาสนา"
        bloodGroup comment: "หมู่เลือด"
        personnelType comment: "ประเภทพนักงาน"
        identificationCard comment: "บัตรประชาชน"
        homeRegistration comment: "ทะเบียนบ้าน"
        shiftWork comment: "ประเภทกะงาน 0 = ปกติ,1 = shift"
        nameTitle comment: "คำนำหน้าชื่อ นาง นาง นางสาว"
        nameRank comment: "คำนำหน้าชื่อตำแหน่ง"
        status defaultValue:"true",comment: "สถานะ Record"
    }

    def generateIDService

    def beforeInsert(){
        withOutPay = false
        status = true
    }

    def beforeValidate() {
        if(!this.id)
        {
            def cObj = Personnel.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}PER_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}PER_${sprintf('%05d', idCount+1)}"
        }
    }
}