package hrportal

class BloodGroup {

    String id
    String code
    String name
    Boolean status
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code blank: true,nullable: true
        name blank: false,nullable: false
        status nullable: true
    }

    static mapping = {
        comment "หมู่เลือด"
        id generator: "assigned"
        code comment:"รหัสกรุ๊ปเลือด"
        name comment:"ชื่อหมู่เลือด"
        status defaultValue:"true",comment: "สถานะ Record"
    }

    def generateIDService

    def beforeInsert(){
        status = true
    }

    def beforeValidate() {
        if(!this.id)
        {
            def cObj = BloodGroup.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}REL_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}REL_${sprintf('%05d', idCount+1)}"
        }
    }
}
