package hrportal

class Citizenship {

    String id
    String code
    String name
    Boolean status
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code blank: true,nullable: true
        name blank: false,nullable: false
        status nullable: true
    }

    static mapping = {
        comment "สัญชาติ"
        id generator: "assigned"
        code comment:"รหัส"
        name comment:"ชื่อสัญชาติ"
        status defaultValue:"true",comment: "สถานะ Record"
    }

    def generateIDService

    def beforeInsert(){
        status = true
    }

    def beforeValidate() {
        if(!this.id)
        {
            def cObj = Citizenship.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}CIT_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}CIT_${sprintf('%05d', idCount+1)}"
        }
    }
}
