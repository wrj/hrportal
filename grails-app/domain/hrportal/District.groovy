package hrportal

class District {

    static auditable = true
    String id
    String districtName
    Province province
    Boolean status
    Date dateCreated
    Date lastUpdated

    static constraints = {
        districtName blank: false,nullable: false
        province nullable: true
        status nullable: true,blank: true
    }

    static mapping = {
        comment "เขต"
        id generator: "assigned"
        districtName comment:"ชื่อเขต"
        province comment:"รหัสจังหวัด"
        status defaultValue:"true",comment: "สถานะ Record"
    }

    def generateIDService

    def beforeInsert(){
        status = true
    }

    def beforeUpdate(){

    }

    def beforeValidate() {
        if(!this.id)
        {
            def cObj = District.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}DIS_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}DIS_${sprintf('%05d', idCount+1)}"
        }
    }
}
