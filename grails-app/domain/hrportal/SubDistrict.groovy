package hrportal

class SubDistrict {

    static auditable = true
    String id
    String subDistrictName
    District district
    Boolean status
    Date dateCreated
    Date lastUpdated

    static constraints = {
        subDistrictName blank: false,nullable: false
        district nullable: false
        status nullable: true,blank: true
    }

    static mapping = {
        comment "แขวง"
        id generator: "assigned"
        subDistrictName comment:"ชื่อแขวง"
        district comment:"รหัสเขต"
        status defaultValue:"true",comment: "สถานะ Record"
    }

    def generateIDService

    def beforeInsert(){
        status = true
    }

    def beforeUpdate(){

    }

    def beforeValidate() {
        if(!this.id)
        {
            def cObj = SubDistrict.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}SDIS_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}SDIS_${sprintf('%05d', idCount+1)}"
        }
    }
}
