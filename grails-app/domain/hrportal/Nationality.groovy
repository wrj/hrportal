package hrportal

class Nationality {

    static auditable = true
    String id
    String code
    String name
    Boolean status
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code blank: true,nullable: true
        name blank: false,nullable: false
        status nullable: true
    }

    static mapping = {
        comment "เชื้อชาติ"
        id generator: "assigned"
        code comment:"รหัส"
        name comment:"ชื่อเชื้อชาติ"
        status defaultValue:"true",comment: "สถานะ Record"
    }

    def generateIDService

    def beforeInsert(){
        status = true
    }

    def beforeValidate() {
        if(!this.id)
        {
            def cObj = Nationality.last()
            def idCount = cObj?cObj.getId().replaceAll("${generateIDService.getServiceId()}NAT_", " ").toInteger():0
            this.id = "${generateIDService.getServiceId()}NAT_${sprintf('%05d', idCount+1)}"
        }
    }
}
