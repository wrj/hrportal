package hrportal

import grails.transaction.Transactional
import grails.web.context.ServletContextHolder
import groovy.io.FileType

@Transactional
class UploadService {

    def servletContext = ServletContextHolder.servletContext
    def uploadResource = servletContext.getRealPath("/uploadresource")
//    def uploadResource = "/home/cccadmin/uploadResult"

    def getPathResource()
    {
        return uploadResource
    }

    def uploadFile(request,String fieldName,String pathFile,String prefixName) {
        // Create a File object representing the folder
        def (folder,fileName) = [new File(uploadResource+"${pathFile}"),""]
        // If it doesn't exist
        if( !folder.exists() ) {
            // Create all folders up-to and including B
            folder.mkdirs()
        }
        def f = request.getFile(fieldName)
        if(!f.empty){
            def oriFileName = f.originalFilename.split("\\.")
            fileName = "${prefixName}_${oriFileName[0].replaceAll(" ","")}_${UUID.randomUUID().toString().replaceAll("-","")}.${oriFileName[1]}"
            def fullPath = uploadResource+"${pathFile}/${fileName}"
            f.transferTo(new File(fullPath))
        }
        return fileName
    }

    def deleteFile(String fieldName,String pathFile)
    {
        boolean fileSuccessfullyDeleted = new File("${uploadResource}${pathFile}/${fieldName}").delete()
    }

    def listFileInFolder(String pathFile)
    {
        def list = []
        def dir = new File("${uploadResource}/${pathFile}")
        if( dir.exists() ) {
            dir.eachFile () { file ->
                def oriFileName = file.getName().split("\\.")
                if(oriFileName[0] != "")
                {
                    list << file.getName()
                }
            }
        }
        return list
    }
}
