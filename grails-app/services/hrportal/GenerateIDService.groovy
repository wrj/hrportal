package hrportal

import grails.transaction.Transactional

@Transactional
class GenerateIDService {

    private prefixId = "L"

    def serviceMethod() {

    }

    @Transactional
    public String getServiceId() {
//        def counterObj = Counter.findByTableName(tableName)
//        def number = counterObj.getRunNumber()
//        if (number) {
//            number = 1
//        }else{
//            number += 1
//        }
//        def idRunning = "${prefixId}_${sprintf('%05d', number)}"
        return prefixId
    }

    public String nullToBlank(String dataValue) {
        String result = ""
        if(dataValue)
        {
            result = dataValue
        }
        return result
    }
}