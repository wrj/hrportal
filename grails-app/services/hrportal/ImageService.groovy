package hrportal

import grails.transaction.Transactional
import grails.web.context.ServletContextHolder
import sun.misc.BASE64Encoder

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

@Transactional
class ImageService {

    def servletContext = ServletContextHolder.servletContext
    def uploadResource = servletContext.getRealPath("/uploadresource")
//    def uploadResource = "/home/cccadmin/uploadResult"

    def serviceMethod() {

    }

    def createImage(String imageName,String imageString,String filepath){
        // Create a File object representing the folder
        def folder = new File(uploadResource+"/"+filepath)
        // If it doesn't exist
        if( !folder.exists() ) {
            // Create all folders up-to and including B
            folder.mkdirs()
        }
        def base64Str = imageString
        byte[] btDataFile = new sun.misc.BASE64Decoder().decodeBuffer(base64Str);
        File of = new File(uploadResource+"/"+filepath+"/" + imageName);
        FileOutputStream osf = new FileOutputStream(of);
        osf.write(btDataFile);
        osf.flush();
    }

    def readImage(String imageName,String filePath) {
        String imageString = ""
        def getContentType = org.apache.commons.lang.StringUtils.substringAfter(imageName,".")
        def File = new File(uploadResource+"/"+filePath+"/" + imageName)
        if(File.exists())
        {
            BufferedImage img = ImageIO.read(new File(uploadResource+"/"+filePath+"/" + imageName));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(img, getContentType , bos);
            byte[] imageBytes = bos.toByteArray();
            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);
            bos.close();
        }
        return imageString
    }

    def deleteImage(String imageName,String filePath) {
        boolean fileSuccessfullyDeleted = new File(uploadResource + "/" + filePath + "/" + imageName).delete()
    }

}
