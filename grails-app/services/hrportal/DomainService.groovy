package hrportal

import grails.transaction.Transactional

@Transactional
class DomainService {

    def serviceMethod() {

    }

    def convertDomainToMap(LinkedHashMap data,domainObj) {
        domainObj.collect{
            if(it.key != 'generateIDService')
            {
                if(it.value.getClass() == java.lang.String) {
                    data[it.key] = it.value?:""
                } else if(it.value.getClass() == java.lang.Integer) {
                    data[it.key] = it.value ?: "0"
                } else if(it.value.getClass() == java.lang.Double) {
                    data[it.key] = it.value?:"0.0"
                } else if(it.value.getClass() == java.lang.Boolean) {
                    data[it.key] = it.value?:false
                }else{
                    data[it.key] = it.value?:""
                }
            }
        }
        return data
    }

}
