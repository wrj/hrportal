package hrportal

import grails.transaction.Transactional

import java.text.SimpleDateFormat

@Transactional
class DataService {

    def convertStringToDate(dataInput,String fieldName) {
        if (dataInput.containsKey(fieldName) && dataInput[fieldName] != "") {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH)
            dataInput[fieldName] = df.parse(dataInput[fieldName])
        }
        return dataInput
    }

    def checkKey(dataInput,String fieldName) {
        def result = false
        if (dataInput.containsKey(fieldName) && dataInput[fieldName] != "") {
            result = true
        }
        return result
    }

    def checkKeySize(dataInput,String fieldName) {
        def result = false
        if (dataInput.containsKey(fieldName) && dataInput[fieldName].size() != 0) {
            result = true
        }
        return result
    }
}
