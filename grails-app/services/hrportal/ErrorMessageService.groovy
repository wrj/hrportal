package hrportal

import grails.transaction.Transactional

@Transactional
class ErrorMessageService {

    def messageSource

    def serviceMethod(List allErrors) {
        def messageReturn = ""
        allErrors.each {
            if(messageReturn == "")
            {
                messageReturn = messageSource.getMessage(it, null)
            }else{
                messageReturn = "${messageReturn} \n ${messageSource.getMessage(it, null)}"
            }
        }
        return messageReturn
    }
}
