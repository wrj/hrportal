/**
 * Created by piyawat on 5/13/2016 AD.
 */
grails.json.legacy.builder = true
swaggydoc {
    description = "Contact email to worajedt.sit@chulabhornhospital.com"
    title = "Chulabhorn Hospital HRIS API References"
    apiVersion = "1.0"
}
grails.reload.enabled = true
grails.plugin.databasemigration.updateOnStart = true

grails.mime.types = [
        json:['application/json', 'text/json']
]